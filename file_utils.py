import output_printer as printer
from output_printer import OutputType
import zipfile


def get_file_contents(filename: str) -> list:
    printer.print("Retrieving lines from file: {0}", OutputType.INFO, [filename])
    try:
        with open(filename) as f:
            return f.read().splitlines()
    except FileNotFoundError:
        handle_file_not_found(filename)


def get_zipped_file(filename: str) -> zipfile.ZipFile:
    printer.print("Loading zipfile: {0}", OutputType.INFO, [filename])
    try:
        return zipfile.ZipFile(filename)
    except FileNotFoundError:
        handle_file_not_found(filename)


def handle_file_not_found(filename: str):
    printer.print("File not found: {0}", OutputType.FAIL, [filename])