from rich.console import Console
from enum import Enum, auto


class OutputType(Enum):
    INFO = auto()
    SUCCESS = auto()
    FAIL = auto()
    VALUE = auto()


LINE_PREFIXES_BY_OUTPUT_TYPE = {
    OutputType.INFO: "[bold yellow][*][/]",
    OutputType.SUCCESS: "[bold green][+][/]",
    OutputType.FAIL: "[bold red][-][/]",
    OutputType.VALUE: "[bold magenta][$][/]"
}


ARGUMENT_TEMPLATE = "[bold blue]{0}[/]"


console = Console()


def print(message: str, type: OutputType, args: list = None):
    prefix: str = get_prefix(type)
    if (args is not None):
        message = get_formatted_message(message, args)
    console.print(f'{prefix} {message}')


def get_prefix(type: OutputType) -> str:
    return LINE_PREFIXES_BY_OUTPUT_TYPE.get(type)


def get_formatted_message(message: str, args: list) -> list:
    formatted_args: list = get_formatted_arguments(args)
    return message.format(*formatted_args)


def get_formatted_arguments(args: list) -> list:
    return [ARGUMENT_TEMPLATE.format(arg) for arg in args]