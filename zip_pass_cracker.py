#!/usr/bin/env python3

import output_printer as printer
from output_printer import OutputType
import zipfile
import optparse
import file_utils
from concurrent.futures import ThreadPoolExecutor, as_completed
import util
from parser_config import ParserConfig, ParserOptionItem


PARSER_USAGE = "zip_pass_cracker -f <zipfile> -d <dictionary>"
PARSER_OPTIONS = [
    ParserOptionItem("-f", "zipfile", "string", "specify zip file"),
    ParserOptionItem("-d", "dictionary", "string", "specify dictionary file")
]
PARSER_CONFIG = ParserConfig(PARSER_USAGE, PARSER_OPTIONS)


def main():
    (zipfile_name, dictionary) = get_arguments()
    password_candidates: list = get_password_candidates(dictionary)
    zipped_file = get_zipped_file(zipfile_name)
    crack_file(zipped_file, password_candidates)


def get_arguments() -> tuple:
    parser = util.get_parser(PARSER_CONFIG)
    (options, args) = parser.parse_args()
    if (options.zipfile == None) or (options.dictionary == None):
        printer.print("usage: {0}", OutputType.VALUE, [parser.usage])
        util.quit("Exiting", 0)
    else:
        return (options.zipfile, options.dictionary) 


def get_password_candidates(filename: str) -> list:
    candidates: list = file_utils.get_file_contents(filename)

    if (candidates == None):
        util.quit("Exiting", -1)

    if (len(candidates) == 0):
        util.quit("Dictionary file is empty. Exiting", -1)

    return candidates


def get_zipped_file(filename: str) -> zipfile.ZipFile:
    zipped_file: zipfile.ZipFile = file_utils.get_zipped_file(filename)

    if (zipped_file == None):
        util.quit("Exiting", -1)

    return zipped_file


def crack_file(file: zipfile.ZipFile, password_candidates: list):
    printer.print("Attempting to crack file: {0}", OutputType.INFO, [file.filename])
    password_found = create_and_run_threads(file, password_candidates)

    if (password_found):
        util.quit("Exiting", 0)

    util.quit("Found no working passwords from dictionary. Exiting", -1)


def create_and_run_threads(file: zipfile.ZipFile, password_candidates: list) -> bool:
    password_found = False

    with ThreadPoolExecutor(max_workers=len(password_candidates)) as executor:
        futures = {executor.submit(extract_file, file, item): item for item in password_candidates}
        for future in as_completed(futures):
            password = futures[future]
            is_correct_pass = future.result()
            if (is_correct_pass):
                password_found = True
                printer.print("Found working password: {0}",
                    OutputType.VALUE, [password])

    return password_found


def extract_file(file: zipfile.ZipFile, password: str) -> bool:
    try:
        file.extractall(pwd=password.encode())
        return True
    except:
        return False


if __name__ == "__main__":
    main()